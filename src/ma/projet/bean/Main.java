package ma.projet.bean;

import java.util.Date;

import ma.projet.Personne;

public class Main {

	public static void main(String[] args) {
		
		Personne dev1 = new Developpeur("Albert", "Einsten", "albert@gmail.com", "0606060606", 2000, "Java", new Date("1986/04/02"));
		Personne dev2 = new Developpeur("Mister", "Magoo", "magoo@gmail.com", "0606060607", 2420, "PHP",new Date("1986/04/02"));
		
		Personne mang1 = new Manager("Jean","Baton","baton@gmail.com", "0606060608", 2220, "informatique",new Date("1986/04/02"));
		
		System.out.println(dev2);
		System.out.println(mang1);		
		

	}

}
