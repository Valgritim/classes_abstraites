package ma.projet.bean;


import java.text.SimpleDateFormat;
import java.util.Date;


import ma.projet.Personne;

public class Developpeur extends Personne {
	
	private String specialite;

	public Developpeur(String lastName, String firstName, String mail, String phone, int salary,
			String specialite ,Date birthDate) {
		super(id, lastName, firstName, mail, phone, salary, birthDate);
		this.specialite = specialite;
	}

	public String getSpecialite() {
		return specialite;
	}

	public void setSpecialite(String specialite) {
		this.specialite = specialite;
	}


	@Override
	public String toString() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("d MMMM yyyy");
		String date = dateFormat.format(getBirthDate());
		return "Le salaire du developpeur " + getLastName() + " " + getFirstName() + ", date de naissance: " + date + ", salaire: " + calculerSalaire() + " euros, sa spécialité: " + getSpecialite();
	}

	@Override
	public double calculerSalaire() {
		double salary = this.getSalary();
		salary = salary * 1.20;
		return salary;
	}

}
