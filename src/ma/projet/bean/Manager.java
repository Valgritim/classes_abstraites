package ma.projet.bean;


import java.text.SimpleDateFormat;
import java.util.Date;


import ma.projet.Personne;

public class Manager extends Personne{

	private String service;

	public Manager(String lastName, String firstName, String mail, String phone, int salary, String service, Date birthDate) {
		super(id, lastName, firstName, mail, phone, salary, birthDate);
		this.service = service;
	}
	
	public String getService() {
		return service;
	}



	public void setService(String service) {
		this.service = service;
	}


	@Override
	public String toString() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("d MMMM yyyy");
		String date = dateFormat.format(getBirthDate());
		return "Le salaire du manager " + getLastName() + " " + getFirstName() + ", date de naissance: " + date + ", salaire: " + calculerSalaire() + " euros, son service: " + getService();
	}

	@Override
	public double calculerSalaire() {
		double salary = this.getSalary();
		salary = salary * 1.35;
		return salary;
	}
	
	
}
