package ma.projet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public abstract class Personne {
	
		protected static int id;
		private String lastName;
		private String firstName;
		private String mail;
		private String phone;
		private int salary;
		private static int count;
		private Date birthDate;
		
		
		public Personne(int id, String lastName, String firstName, String mail, String phone, int salary, Date birthDate) {
			super();
			this.id = ++count;
			this.lastName = lastName;
			this.firstName = firstName;
			this.mail = mail;
			this.phone = phone;
			this.salary = salary;
			this.birthDate = birthDate;
		}

		public static int getId() {
			return id;
		}

		public static void setId(int id) {
			Personne.id = id;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getMail() {
			return mail;
		}

		public void setMail(String mail) {
			this.mail = mail;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		public int getSalary() {
			return salary;
		}

		public void setSalary(int salary) {
			this.salary = salary;
		}

		public Date getBirthDate() {
			return birthDate;
		}

		public void setBirthDate(Date birthDate) {
			this.birthDate = birthDate;
		}
		
		

		@Override
		public String toString() {
			SimpleDateFormat dateFormat = new SimpleDateFormat("d MMMM yyyy");
			String date = dateFormat.format(getBirthDate());
			return "Personne [lastName=" + lastName + ", firstName=" + firstName + ", mail=" + mail + ", phone=" + phone
					+ ", salary=" + salary + ", birthDate=" + date + "]";
		}

		public abstract double calculerSalaire();

}
